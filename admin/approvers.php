<?php
 
/*
 * Class for handling approval of profile submissions and amendments
 *
 * @since DTC_Profile_Editor 0.3
 */
class WDTC_Approve_Changes_Pending {

	/*
	 * Set up actions and filters
	 */
	public function init() {
		add_action( 'admin_footer-post-new.php', array(&$this, 'publish_js' ), 11 );
		add_action( 'admin_footer-post.php', array(&$this, 'publish_js' ), 11 );
		add_action( 'changes-pending_to_publish', array(&$this, 'commit_pending_changes'), 10, 2 );
		add_filter( 'redirect_post_location', array(&$this, 'redirect_after_save' ), 10, 2);
	}

	/*
	 * Javascript to power the publish changes dialogue box.
	 */
	function publish_js() {
		if (get_post_status() != 'changes-pending')
			return;
		$title = get_the_title();
		$msg = 'Do you want to apply the changes here to the live version of the post \'' . $title . '\'? The live verstion of the post \'' . $title . '\' will be overwritten with the contents of this one; and this one will be sent to trash.';
		$js = '<script type="text/javascript">';
		$js .= 'jQuery( document ).ready(';
		$js .= '	function( $ ) {';
		$js .= '		$( \'#publish\' ).on(';
		$js .= '			\'click\',';
		$js .= '			function( event ) {';
		$js .= '				if ( $( this ).attr( \'name\' ) !== \'publish\' ) {';
		$js .= '					return;';
		$js .= '				}';
		$js .= '				if ( ! confirm( ' . wp_json_encode( $msg ) . ' ) ) {';
		$js .= '					event.preventDefault();';
		$js .= '				}';
		$js .= '			}';
		$js .= '		);';
		$js .= '	}';
		$js .= ');';
		$js .= '</script>';
		if ( ! wp_script_is( 'jquery', 'done' ) ) {
			return;
		}
		echo $js;
	}

	/*
	 * Commit amendments to a published profile - transfer to parent profile and delete
	 * temporary profile containing changes.
	 *
	 * @param WP_Post $profile the (temporary) profile containing the changes
	 */
	function commit_pending_changes(WP_Post $post) {
		if($post->post_parent) {
			$parent = get_post($post->post_parent);
			$parent_info = array(
      			'ID'           => $parent->ID,
      			'post_title'	=> $post->post_title,
      			'post_content'   => $post->post_content,
				'post_excerpt'   => $post->post_excerpt
  			);
			wp_update_post( $parent_info );
			$taxonomies = get_object_taxonomies($post->post_type);
			foreach ($taxonomies as $taxonomy) {
				$post_terms = wp_get_object_terms($post->ID, $taxonomy, array('fields' => 'slugs'));
				wp_set_object_terms($parent->ID, $post_terms, $taxonomy, false);
			}
			$metadata = get_post_meta($post->ID);
			if ($metadata) {
				foreach ($metadata as $meta_key=>$meta_values) {
					if (!preg_match('/^_/',$meta_key) || '_thumbnail_id' == $meta_key) {
						foreach ($meta_values as $meta_value) {
							update_post_meta($parent->ID,$meta_key,$meta_value);
						}
					}
				}
			}
			$post_info = array(
      			'ID'           => $post->ID,
      			'post_status'   => 'trash'
  			);
			wp_update_post( $post_info );
		}	
	}
	
	/*
	 * After saving commiting profile amendment (i.e. 'changes-pending' post) and moving
	 * it to trash, redirect to to the profile to which the changes have been committed
	 *
	 * @param string @location url to redirect
	 * @param int @post_id id of the post saved
	 *
	 * @return string url to redirect to.
	 */
	function redirect_after_save( $location, $post_id ) {
		$post_type = get_post_type($post_id);
        if ( 'changes-pending' == get_post_status( $post_id ) && is_admin() ) {
        	$parent_id = wp_get_post_parent_id($post_id);
        	if ($parent_id)
        		$location = admin_url( 'post.php?post=' . $parent_id . '&action=edit' );
        	else
	            $location = admin_url( 'edit.php?post_type=' . $post_type . '&all_posts=1' );
        }
        return $location;
    }
}