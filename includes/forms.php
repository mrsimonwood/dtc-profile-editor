<?php
/*
 * Class for the form
 *
 * @since DTC_Profile_Editor 0.4
 */

class WDTC_Edit_Post_Form {

	/*
	 * Array of WDTC_Attribute objects - the fields for which data can be added.
	 *
	 * @var array
	 */
	private $fields;
	
	/*
	 * Stores validation error on form submission.
	 *
	 * @var string
	 */
	private $error;

	/*
	 * Constructor
	 *
	 * @param string $id identifier for the field
	 * @param string $name well formatted name to display publicly
	 * @param string $base_url the base URL
	 * @param string $usernameprefix the prefix for usernames (eg. @ for Twitter)
	 */
    public function __construct(array $fields = array()) {
      	add_shortcode( 'wdtc-edit-' . $this->get_type() . '-form', array(&$this, 'handle_form' )); // shortcode for displaying the form
		wp_enqueue_style('jquery-style', 'https://ajax.googleapis.com/ajax/libs/jqueryui/1.8.2/themes/smoothness/jquery-ui.css'); 	
		$this->set_fields($fields);
    }

	/*
	 * Set up action hooks
	 */
	public function init() {
    	$this->register_changes_pending_post_status();
		add_action('wp_enqueue_scripts', array(&$this, 'dtc_profile_editor_css'));
	}

	/*
	 * Setter for fields
	 *
	 * @param array $fields WDTC_Attribute objects
	 */
	public function set_fields($fields) {
		$this->fields = apply_filters('wdtcpe_set_fields', $fields);
	}

	/*
	 * Getter for fields
	 *
	 * @return array WDTC_Attribute objects
	 */
	private function get_fields() {
		return $this->fields;
	}

	/*
	 * Register new post status for changes pending, used when amendments are
	 * submitted to be applied to an existing post (once approved).
	 */
	public function register_changes_pending_post_status() {
		$archive_registerer = new WDTC_Post_Status_Registerer( 'changes-pending', 'Changes Pending', 'Changes Pending', array($this->get_type()));
		$archive_registerer->init();
	}

	/*
	 * Returns post type the form edits. Override for use with different post types.
	 *
	 * @return string post type
	 */
	protected function get_type() {
		return 'post';
	}

	/*
	 * Gets the post_id for the post the form edits. Override with suitable logic.
	 *
	 * @return int post ID, NULL for a new post
	 */
	protected function get_post_to_edit_id() {
		return NULL;
	}

	/*
	 * Gets the object id for for the object the fields are associated with. Override with suitable logic.
	 *
	 * @return int ID
	 */
	protected function source_id_for_field() {
		return NULL;
	}
	
	/*
	 * The message shown when the viewer is not logged in.
	 *
	 * @return string instruction to log in
	 */
	protected function login_message() {
		return "To edit a post, first sign in";
	}

	/*
	 * Custom CSS for the plugin.
	 */
	public function dtc_profile_editor_css() {
		wp_enqueue_style('wdtcpe-style', plugins_url('style.css' , dirname(__FILE__)));
	}
	
	/*
	 * Set up client-side validation for the fields.
	 */
	public function validator() {
		if (class_exists('WDTC_Attribute')) {
 			wp_enqueue_script('jquery-validate', plugin_dir_url( __FILE__ ) . 'js/jquery.validate.min.js', array('jquery'), '1.10.0', true);   	
			$patterns = array();
			echo '<script type="text/javascript">';
			$fields = apply_filters('wdtc_validate_fields_setup', $this->get_fields());
			foreach ($fields as $field) {
				if ($field->get_validation_pattern())
					$patterns[$field->get_validation_pattern(false)->get_id()] = $field->get_validation_pattern(false);
				if ($field->validation_rules()) {
					$rules[] = '"' . $field->get_id() . '": {' . $field->validation_rules() . '}';
					if ($field->validation_message())
						$messages[] = '"' . $field->get_id() . '": "' . $field->validation_message() . '"';
				}
			}
			echo '	jQuery(document).ready(function($) {';
			foreach ($patterns as $pattern) {
				echo $pattern->pattern_jQuery();
			}
			echo '	$(\'#wdtcpeform\').validate({';
			echo '      rules: {';
			echo implode(",", $rules);
			echo '		}, ';
			echo '		messages: {';
			echo implode(",", $messages); 
			echo '		}';
			echo '  });';
			echo '});';
			echo '</script>';
		}		    
	}
	
	/*
	 * This is the function that handles the form - whether to display it, process inputs etc.
	 *
	 * @param array $atts attributes from the shortcode, not currently used.
	 *
	 * @return string html
	 */
	public function handle_form( $atts ) {
		if ($this->is_form_submitted() && $this->is_nonce_set()) {
			if ($this->is_form_valid()) {
				return $this->process_form();
			} else {
				return stripslashes($this->error) . $this->display_form();
			}
		} else {
			return $this->display_form();
		}
	}

	/*
	 * Override to return html for anything that should appear before the form
	 */
	protected function before_form() {}

	/*
	 * Override to return html for anything that should appear within the form but before the fields
	 */
	protected function before_fields() {}

	/*
	 * Get the html for the form field for the post title
	 *
	 * @return string html
	 */	
	protected function title_field () {
		$title_field = new WDTC_Custom_Text_Field('title', 'Title');
		if ($this->get_post_to_edit_id())
			$title = get_post_field('post_title', $this->get_post_to_edit_id());
		return $title_field->get_form_field('', $title);
	}

	/*
	 * Get the value to be saved as the post's title
	 *
	 * @return string title
	 */
	protected function title_value() {
		return $_POST['title'];
	}

	/*
	 * Get the html for the form field for the post content
	 *
	 * @return string html
	 */		
	protected function content_field() {
		$content_field = new WDTC_Custom_Textarea_Field('content', $this->content_field_name(), array('width' => '480px', 'height' => '480px'));
		if ($this->get_post_to_edit_id())
			$content = get_post_field('post_content', $this->get_post_to_edit_id());
		return $content_field->get_form_field('', $content);
	}

	/*
	 * Get the value to be saved as the post's content
	 *
	 * @return string title
	 */
	protected function content_value() {
		return $_POST['content'];
	}
	
	/*
	 * Get the name to be used in the label of the field for the post's content
	 *
	 * @return string name for content
	 */
	protected function content_field_name() {
		return ucwords($this->get_type());
	}

	/*
	 * Get the html for the form field for uploading a featured image (thumbnail) for the post
	 *
	 * @return string html
	 */		
	protected function image_field() {
		require_once dirname(__FILE__) . '/image_handler.php';
		$image_handler = WDTCPE_Image_Handler::Instance();
		$html = '	<div class="field-label"><label for="wdtcpe_image_file">' . ucwords($this->get_type()) . ' Image</label></div><br/>';
		$html .= '<div class=\'thumbnail\'>' . get_the_post_thumbnail($this->get_post_to_edit_id(), 'thumbnail') . '</div>';
		$html .= '	At least ' . $image_handler->get_min_width() . ' pixels wide and ' . $image_handler->get_min_height() . ' pixels high. ' . $image_handler->get_max_upload_size(true) . ' maximum file size.<br/>';  
		$html .= '	<input type="file" size="60" name="wdtcpe_image_file" id="wdtcpe_image_file"><br/>';
		return $html;
	}
	
	/*
	 * Taxonomy terms for the featured image uploaded for the post
	 *
	 * @return array taxonomy as the key, array of terms as the value
	 *
	 */
	protected function image_taxonomy_terms() {
		return array();
	}
	
	/*
	 * Display the form
	 *
	 * @return string html
	 */
	private function display_form() {
    	if(!is_user_logged_in())
    	{
			if (class_exists('WDTC_Login_Buttons'))	{
				$login_buttons = new WDTC_Login_Buttons(get_permalink(),'Sign in to submit a ' . $this->get_type() . ':');
	    		$login_buttons->init();
	    		$login_buttons->set_redirect(get_permalink());
   				$html = $login_buttons->buttons(false);	
   			} else {
	     		if (function_exists ('shibboleth_login_form'))
		 			$with  = " with your institutional login";
				$html = '<p><a href="' . wp_login_url( get_permalink() . '#wdtcpe') . '" title="Log in">' . $this->login_message() . $with . '</a>.</p>';
			}
     	} else { 
       		$post_id = $this->get_post_to_edit_id();
			$html .= $this->before_form();
			$html .= '<form action="" method="post" name="wdtcpeform" id="wdtcpeform" class="wdtcpeform" enctype="multipart/form-data" >';
			$html .= wp_nonce_field(basename( __FILE__ ),'wdtcpe_nonce', true, false);
			$html .= '	<input type="hidden" name="home" value="' . get_permalink() . '" > ';
			$html .= '	<input type="hidden" name="action" value="wdtcpe_save_submission"> ';
			$html .= '	<input type="hidden" name="task" value="' . $_REQUEST['task'] . '">';
			$html .= '	<input type="hidden" name="postid" id="postid" value="">'; // value = post id for editing
			$html .= $this->before_fields();
			if ($this->get_fields()) {
				foreach($this->get_fields() as $field) {
					$value = $_POST[$field->get_id()];
					$html .= $field->get_form_field($this->source_id_for_field(), $value);
				}
			}
			$html .= $this->title_field();
			$html .= $this->content_field();
			$html .= $this->image_field();
			$html .= '	<p>';
			$html .= '		<input type="submit"   name="user_post_submit" id="user_post_submit" value="Submit">';
			$html .= '		<input type="reset" value="Cancel"  name="cancel" id="cancel" onclick="location.href=' . get_permalink() . '">';
			$html .= '	</p>';
			$html .= '</form>';
		}
		return $html;	
	}

	/*
	 * Process the form, validating and saving field values.
	 *
	 * @return string html
	 */
	private function process_form() {
	   	$current_user = wp_get_current_user();
	   	$user_id = $current_user->ID;
    	if(isset($_POST['action']) && $_POST['action']=="wdtcpe_save_submission")
    	{
			$post_info = array(
				 'post_content' => $this->content_value(),
				 'post_author' => $user_id,
				 'post_title' => $this->title_value()
			);
			$post_id = $this->get_post_to_edit_id();
			if ($post_id && get_post_status($post_id)) {
				$post_author_id = get_post_field( 'post_author', $post_id );
				if ($post_author_id == $user_id) {
					if (get_post_status($post_id) == 'pending' || get_post_status($post_id) == 'changes-pending') {
						$submission_type = '';
						$post_info['ID'] = $post_id;
						$post_id = wp_update_post($post_info);
						$template = 'a further %s update.';
					} else {
						$post_info['post_parent'] = $post_id;
						$post_info['post_type'] = $this->get_type();
						$post_info['post_status'] = 'changes-pending';
						$post_id = $this->insert_post($post_info, $user_id);
						$template = 'an updated %s.';
					}
				} else {
					$post_info['post_content'] = 'Post author of post id ' . $post_id . ' is ' . $post_author_id . ' which does not match the user_id ' . $user_id . ' so the update fails.'; 
					$post_id = 0; // the current user is not the author of the post to be edited, so update fails
					$template = 'an updated %s (permissions issue).';
				}
			} else {
				$post_info['post_type'] = $this->get_type();
				$post_info['post_status'] = 'pending';
				$post_id = $this->insert_post($post_info, $user_id);
				$template = 'a new %s.';
			}
			$message = sprintf($template, $this->get_type());
			if ($post_id) {
				if ($this->get_fields()) {
					foreach($this->get_fields() as $field) {
						$field->save_field_value($this->source_id_for_field());
					}
				}
				// Process the image
				if(is_uploaded_file($_FILES['wdtcpe_image_file']['tmp_name'])) {
					require_once dirname(__FILE__) . '/image_handler.php';
					$image_handler = WDTCPE_Image_Handler::Instance();
					$image_results = $image_handler->process_image('wdtcpe_image_file', $post_id, $this->title_value(), $this->image_taxonomy_terms());
					if ($image_results['error'])
						$image_message = '<p>However, we could not upload the image: ' . $image_results['error'] . ' Please <a href="">try again</a> or email an image to <a href="mailto:' . get_bloginfo('admin_email') . '">' . get_bloginfo('admin_email') . '</a> and we will add it to the ' . $this->get_type() . '</p>';
				}				
				$this->additional_stuff_when_form_saved($post_id, $user_id);
				$subject = $email_message = $_POST['user_name'] . ' submitted ' . $message;
				if ($image_results['ID'])
					$email_message .= " With an image."; 
				$message = '<p>Thank you, you have sucessfully submitted ' . $message . ' This will appear on our website soon.</p>' . $image_message;
				$link = admin_url( sprintf(get_post_type_object($this->get_type())->_edit_link . '&action=edit', $post_id ));
			} else {
				$subject = 'SUBMIT FAIL: ' . $_POST['user_name'] . ' attempted to submit ' . $message;
				$message = '<p>Unfortunately there was a problem saving the ' . $this->get_type() . '.</p>';
				$email_message = $_POST['user_name'] . ' attempted to submit ' . sprintf($email_template, $this->get_type()) . ' but this was not succesful. Details: ';
				foreach($post_info as $key => $value) {
					$email_message .= $key . ' - ' . $value . '; ';
				}
				$email_message .= 'Post update type: ' . $template;
				$link = '';
			}
			$this->send_email($subject,$email_message,$link);
			return $message;
		}	
	}

	/*
	 * Override to do anything additional when the form contents are saved.
	 */
	protected function additional_stuff_when_form_saved($post_id, $user_id) {}

	/*
	 * Create a new post.
	 */	
	protected function insert_post ($post_info, $user_id) {
		return wp_insert_post( $post_info );
	}
	
	/*
	 * Validate each field within the form.
	 *
	 * @return bool false if any field is invalid, otherwise true
	 */
	private function is_form_valid() {
		if (class_exists('WDTC_Attribute')) {
			$fields = apply_filters('wdtc_validate_fields', $this->get_fields());
			foreach($fields as $field) {
				$this->error = $field->validation_error();
				if ($this->error)
					return false;
			}
		}
		return true;
	}
	
	/*
	 * Check if the form has been submitted already
	 *
	 * @return bool true if the form has been submitted
	 */
	private function is_form_submitted() {
		if( isset($_POST['action']) && $_POST['action']=="wdtcpe_save_submission" )
			return true;
	    else
	    	return false;
	}
	
	/*
	 * Has the nonce been set and can it be verified?
	 *
	 * @return bool true if set and verified, otherwise false
	 */
	private function is_nonce_set() {
    		if ( isset( $_POST['wdtcpe_nonce'] ) && wp_verify_nonce( $_POST['wdtcpe_nonce'], basename( __FILE__ ) ) )
    		{
    			return true;
    		}
    		return false;	
	}
	
	/*
	 * Send email to admin when a new post or amendment is submitted.
	 *
	 * @param object $subject the subject line for the email message
	 * @param string $message the content for the email message
	 * @param string $url the address of the post that has been submitted
	 */
    private function send_email($subject,$message,$url = '') 
    {	
		if ($url)
			$message .= "\n\nLink: " . $url;
		$message .= "\n\nLog in to " . get_bloginfo('name') . " to view pending submissions " . wp_login_url(admin_url( 'edit.php?post_type=' . $this->get_type()));
		$recipient = get_bloginfo('admin_email');
		wp_mail($recipient, $subject, $message);
	}
} 
