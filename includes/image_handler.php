<?php
class WDTCPE_Image_Handler {

	/*
	 * Constructor
	 */
	private function __construct() {
		define('MAX_UPLOAD_SIZE', 512000); //500kb
		define('MIN_WIDTH', 150);
		define('MIN_HEIGHT', 150);
		define('TYPE_WHITELIST', serialize(array(
		  'image/jpeg',
		  'image/png',
		  'image/gif'
		  )));
		define('MEDIA_TAXONOMY','media_category');
		define('PROFILE_TAXONOMY_TERM', 'student-profile');
	}
	
	/*
	 * Getter for minimum width
	 *
	 * @return int the minimum image width
	 */
	public function get_min_width() {
		return MIN_WIDTH;
	}
	
	/*
	 * Getter for minimum height
	 *
	 * @return int the minimum image height
	 */
	public function get_min_height() {
		return MIN_HEIGHT;
	}
	
	/*
	 * Getter for the maximum upload size
	 *
	 * @param bool $formatted whether to return the int value, or a string formatted in B/KB/MB etc. 
	 *
	 * @return int the maximum size permissible for the uploaded file
	 */
	public function get_max_upload_size($formatted = false) {
		if ($formatted)
			return $this->format_bytes(MAX_UPLOAD_SIZE);
		return MAX_UPLOAD_SIZE;
	}
	
    /*
     * Call this method to get singleton
     *
     * @return WDTCPE_Image_Handler
     */
    public static function Instance()
    {
        static $inst = null;
        if ($inst === null) {
            $inst = new WDTCPE_Image_Handler();
        }
        return $inst;
    }
    	
	/*
	 * Save uploaded image to media gallery as appropriate.
	 *
	 * @param string $file the name of the image file
	 * @param int $post_id the the post id that the image should be attached to (as thumbnail)
	 * @param string $title the title of the image
	 * @param string $caption text for the image caption
	 *
	 * @return array with error or post id for image attachment
	 */
	public function process_image($file, $post_id, $title, $taxonomy_terms = array(), $caption = ''){
		$error = $this->parse_file_errors($_FILES[$file]);
		if($error){
			return array('error'=>$error);
		} else {
			require_once( ABSPATH . 'wp-admin/includes/image.php' );
			require_once( ABSPATH . 'wp-admin/includes/file.php' );
			require_once( ABSPATH . 'wp-admin/includes/media.php' );
			$attachment_id = media_handle_upload($file, $post_id);
			if ( is_wp_error( $attachment_id ) ) {
				return array('error'=>$attachment_id->get_error_message());
			} else {
				update_post_meta($post_id, '_thumbnail_id', $attachment_id);
				$attachment_data = array(
					'ID' => $attachment_id,
					'post_excerpt' => $caption,
					'post_title' => $title
				);
				wp_update_post($attachment_data);
				update_post_meta($attachment_id, '_wp_attachment_image_alt', $title);
				foreach ($taxonomy_terms as $taxonomy => $terms) {
					if (taxonomy_exists($taxonomy)) {
						foreach ($terms as $term) {
							if (term_exists($term, $taxonomy))
								wp_set_object_terms($attachment_id, $term , $taxonomy);
						}
					}
				}
				return array('id'=>$attachment_id);
			}
		}
	}
	
	/*
	 * Check image to be uploaded is a file of correct type, size and resolution
	 *
	 * @param string $file name of image file
	 *
	 * @return string error message
	 */
	private function parse_file_errors($file = '') {
	  	$error = 0;
	  	if($file['error']){
  			$error = "There was an upload error!";     
	  		if ($file['error'] == UPLOAD_ERR_NO_FILE)
	  			$error = "No file uploaded.";     
			return $result;
	  	}
	  	$image_data = getimagesize($file['tmp_name']);
	  	if(!in_array($image_data['mime'], unserialize(TYPE_WHITELIST))){
			$error = 'Your image must be a jpeg, png or gif!';
	  	}elseif(($file['size'] > MAX_UPLOAD_SIZE)){
			$error = 'Your image was ' . $this->format_bytes($file['size']) . '! It must not exceed ' . $this->format_bytes(MAX_UPLOAD_SIZE) . '.';
	  	}elseif(($image_data[0] < MIN_WIDTH) || ($image_data[1] < MIN_HEIGHT)) {
			$error = 'Your image was too small: ' . $image_data[0] . ' pixels wide and ' . $image_data[1] . ' pixels high. It needs to be at least ' . MIN_WIDTH . ' pixels wide and ' . MIN_HEIGHT . ' pixels high.';  
	  	}
	  	return $error;
	}	
	
	/*
	 * Convert bytes into an alternative unit appropriate to order of magniturde
	 *
	 * @param integer bytes to convert
	 * @param level of precision to round to
	 *
	 * return string value and units
	 */
	public function format_bytes($bytes, $precision = 2) { 
		$units = array('B', 'KB', 'MB', 'GB', 'TB'); 
		$bytes = max($bytes, 0); 
		$pow = floor(($bytes ? log($bytes) : 0) / log(1024)); 
		$pow = min($pow, count($units) - 1); 
		$bytes /= pow(1024, $pow);
		return round($bytes, $precision) . ' ' . $units[$pow]; 
	}
}