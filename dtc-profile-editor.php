<?php
/*
Plugin Name: DTP Frontend Editor 
Bitbucket Plugin URI: https://bitbucket.org/mrsimonwood/dtc-profile-editor
Description: Allows logged in users to submit and edit posts and their fields via a shortcode generated form. Depends on the DTP Posts and Fields plugin.
Version: 0.4.4
Author: Simon Wood
Author URI: http://www.simonwood.info
License: GPL2
*/

/*  Copyright 2013 Simon Wood (email : wp-plugins@simonwood.info)

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License, version 2, as 
    published by the Free Software Foundation.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
*/


/* 
 * This plugin depends on  DTC Posts and Fields so use a hook from that plugin
 * to initialise with the following function
 */
function dtc_frontend_editor_init() {
	require_once dirname( __FILE__ ) . '/includes/forms.php';
	//$edit_post_form = new WDTC_Edit_Post_Form();
	//$edit_post_form->init();
	add_action('admin_init', 'setup_approver');
}
add_action( 'dtc_posts_and_fields_init', 'dtc_frontend_editor_init' );

/* 
 * Set up the WDTC_Approve_Changes_Pending object
 */
function setup_approver() {
	if (current_user_can('publish_posts') ) {
		require_once dirname( __FILE__ ) . '/admin/approvers.php';
		$approver = new WDTC_Approve_Changes_Pending();
		$approver->init();
	}
}