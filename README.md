# README #

### What is this repository for? ###

* Wordpress Plugin to allow logged in users to submit and edit posts and their fields via a shortcode generated form. 
* Version 0.4.4

### How do I get set up? ###

* Depends on the DTP Posts and Fields plugin.
* Upload to wp-content/plugins via FTP or Plugins > Add New > Upload Plugin
* In the dashboard choose "Activate" or "Activate Plugin"